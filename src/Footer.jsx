import { alpha, Box, Button, Grid, makeStyles, Typography } from "@material-ui/core";
import { Facebook, Instagram, Pinterest, Reddit } from "@material-ui/icons";

const useStyles = makeStyles(theme=> ({
    footerContainer:{
        width: "100%",
        borderTop: "2px",
        borderTopStyle: "solid",
        borderTopColor: theme.palette.primary.dark,
        backgroundColor: theme.palette.primary.main,
        
        bottom: 0
    },
    footerContent:{
        padding: theme.spacing(2),
        
    },
    socialsContainer:{
        display: "flex",
        justifyContent: "center"
    },
    socialIcon:{
        width: theme.spacing(5),
        padding: theme.spacing(1.5),
        color: alpha(theme.palette.common.white, 0.65)
    },
    footerBottomSection:{
        color: alpha(theme.palette.common.white, 0.65),
        backgroundColor: theme.palette.primary.dark,
        textAlign: "center",
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1)
    },
    footerSiteName:{
        color: "white"
    },

    footerTitle:{
        color: "white",
        
    },
    footerDescription:{
        marginTop: theme.spacing(1),
        alignSelf: "center",
        width: "50%",
        [theme.breakpoints.down("sm")]:{
            width: "90%"
        }
    }
}));

const Footer = ()=>{

    const classes = useStyles();
    return (
        <footer className={classes.footerContainer}>

            <Box className={classes.footerContent}>
                <Box sx={{display: "flex", flexDirection: "column"}}>
                    <Box sx={{color: "white", fontWeight: '500', letterSpacing: 6}}>
                        <Typography variant="h6" align="center">Find out more about the project</Typography>
                    </Box>
                    
                    
                    <Typography align="center" className={classes.footerDescription}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Cras fringilla lacus et nisi consectetur, vitae luctus risus auctor. 
                        <br/>
                        Ut vestibulum tristique enim et tempor. Curabitur et nisl vel purus blandit interdum vitae vitae tortor. 
                        Nam scelerisque eros eu hendrerit suscipit. 
                    </Typography>
                </Box>
                <div className={classes.socialsContainer}>
                    <Facebook className={classes.socialIcon}/>
                    <Instagram className={classes.socialIcon}/>
                    <Reddit className={classes.socialIcon}/>
                    <Pinterest className={classes.socialIcon}/>
                </div>
            </Box>
            <Typography className={classes.footerBottomSection}>

                    Copyright &copy;{new Date().getFullYear()} <Button variant="text" className={classes.footerSiteName}>joker.com</Button> all rights reserved.
            </Typography>

        </footer>
    )
};

export default Footer;

