import { Box, makeStyles } from '@material-ui/core';
import { useState } from 'react';
import { pullRandom } from './jokeApi';
import JokeCard from './JokeCard';
import logo from './logo.svg';
import Navbar from './Navbar';
import PullJokeButton from './PullJokeButton';
import axios from 'axios';
import Footer from './Footer';

const useStyles = makeStyles( theme=> ({
  jokeCardContainer:{
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  }
}));



function App() {

  const pull = ()=>{
    axios.get("https://v2.jokeapi.dev/joke/Any")
    .then((response)=>{
      console.log(response.data)
      setList([...list, response.data])
    })
    .catch(err=> console.log(err)) 
  }

  

  const [list, setList] = useState([])

  const classes = useStyles();
  return (
    <div className="App">

      <Navbar/>
      
      <Box className={classes.jokeCardContainer}>
        {
          list.map(j => <JokeCard key={j.id} joke={j} />)
        }
        <PullJokeButton click={pull}/>     
      </Box>
      
      <Footer/>
    </div>
  );
}

export default App;
