import { AppBar, Box, Button, IconButton, makeStyles , styles, Toolbar, Typography, alpha} from "@material-ui/core";
import { Height, Info, Menu, MenuBook, MenuOpen } from "@material-ui/icons";
import logo from './clown.png'
const useStyles = makeStyles((theme)=> ({
    logoImg:{
        display: "flex",
        color: "white",
        alignItems: "flex-end"
    },
    toolbar:{
        display: "flex",
        justifyContent: "space-between"
    },
    about:{
        display: "flex",

    },
    buttonContainer:{
        color: "white",
    },
    navbtn:{
        color: alpha(theme.palette.common.white, 0.85),
        paddingRight: theme.spacing(1),
    }
}));




const Navbar = ()=>{
    
    const classes = useStyles();
    return (
            <AppBar position="sticky">

                <Toolbar className={classes.toolbar}>
                    <IconButton className={classes.logoImg}>
                        <img src={logo}  width={48} height={48}/>
                        <Typography variant="h4">Joker</Typography>
                    </IconButton>
                    
                    <div className={classes.buttonContainer}>
                        <Button className={classes.navbtn}>Random</Button>
                        <Button className={classes.navbtn}>Add</Button>
                        <Button className={classes.navbtn}>Scoreboard</Button>
                        <Button className={classes.navbtn}>Find</Button>
                    </div>

                        
                        <Button startIcon={<Info/>} variant="contained">About</Button>
                    
                    
                </Toolbar>
            </AppBar>
        
    )
}

export default Navbar;