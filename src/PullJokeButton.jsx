import { Box, Fab, makeStyles } from '@material-ui/core';
import { Add, Refresh } from '@material-ui/icons';

const useStyles = makeStyles( theme=> ({
    button:{
        position: "absoulte",
        bottom: 0
    }
}));

const PullJokeButton = (props)=>{
    const classes = useStyles();
    return (
        <div className={classes.button}>   
        <Fab color="primary" onClick={props.click}>
            <Refresh/>
        </Fab>
        </div>
    )
}

export default PullJokeButton