import { alpha, Box, Button, Card, CardActions, CardContent, CardHeader, makeStyles, Typography } from "@material-ui/core";
import { Category, Flag, Info, ThumbDown, ThumbUp } from "@material-ui/icons";
import { useState } from "react";
import { like, dislike } from "./jokeApi";

const useStyles = makeStyles( theme=> ({
    boxContainer:{
        padding: theme.spacing(2),
        maxWidth: 600,
        width: "65%",
        [theme.breakpoints.down("sm")]:{
            width: "85%"
        }
    },
    cardButtons:{
        display: "flex",

    },
    category:{
        fontSize: 14,
     
    }
}))



const JokeCard = (props)=>{
    const [state, setState] = useState({
        liked: false,
        disliked:false,
        reported: false,
    })

    const handleLike = ()=>{  
        //state.liked is passed to enable undoing like 
        like(!state.liked, props.id ,()=> {  
            setState(   {...state, liked: !state.liked})
        });
    }

    const handleDislike = ()=>{
        //state.liked is passed to enable undoing dislike 
        dislike(!state.liked, props.id, ()=> {  
            setState(   {...state, liked: !state.liked})
        });
    }

    const handleReport = ()=>{

    }

    const revealMoreInfo = ()=>[

    ]

    const classes = useStyles();
    return (
        <Box className={classes.boxContainer}>
            <Card variant="outlined">   
                <CardContent>
                    <Box sx={{display: "flex", justifyContent: "space-between", alignItems: "baseline"}}>
                        <Typography variant="h5" gutterBottom >Joke #{props.joke.id}</Typography>
                        <Button size="small" startIcon={<Info/>} variant="outlined" onClick={revealMoreInfo}>Info</Button>
                    </Box>
                    <Typography className={classes.category} color="textSecondary">Category: {props.joke.category}</Typography>
                    <hr/>
                    <Typography variant="body1">{props.joke.setup}</Typography>
                    <br/>
                    <Typography variant="body1">{props.joke.delivery}</Typography>
                    <hr/>
                </CardContent>
                <CardActions className="cardButtons">
                    <Button size="small" 
                        startIcon={<ThumbUp 
                            color={!state.liked ? "action" : "primary"}/> } 
                        onClick={handleLike} >
                            {props.joke.likes}
                    </Button>
                    <Button size="small"
                        startIcon={<ThumbUp 
                            color={!state.disliked ? "action" : "primary"}/> }                                         
                        onClick={handleDislike} >
                        {props.joke.dislikes}
                    </Button>
                    <Button size="small" 
                        onClick={handleReport}
                        startIcon={<Flag/>} sx={{alignSelf: "flex-end"}}>Report joke</Button>
                </CardActions>
            </Card>
        </Box>
    )
}
export default JokeCard