
const axios = require('axios')
const hostname = "http://localhost:2000"


const pullRandom = ()=>{
    let data = {};
    axios.get(`${hostname}/random`)
    .then((response)=> 
    {
        console.log(response.data)
        data = Object.assign({}, response.data);
        console.log(data)
    })
    .catch(err=> console.log(err));
    
    return data;
}

const like = (val, id, callback)=>{

    callback();
}

const dislike = (val, id, callback)=>{

    callback();
}
export {like, dislike, pullRandom}